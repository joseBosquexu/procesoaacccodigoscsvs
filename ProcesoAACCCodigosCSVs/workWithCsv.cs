﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using WDLogger;

namespace ProcesoAACCCodigosCSVs
{
    public static class workWithCsv
    {
 
        /// <summary>
        /// Genera un fichero CSV a partir de una lista de datos.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Lista con los datos a exportar</param>
        /// <param name="filePath">Ruta compleat del fichero a generar</param>
        /// <param name="propertyNames">listado de columnas a mostrar, deben pertenecer a la lista de datos, por defecto se muestran todas las columnas</param>
        /// <param name="strSeparator">Separador a utilizar, por defecto ";"</param>
        public static int WriteToCsv<T>(IList<T> list, string filePath, string[] propertyNames = null, string strSeparator=";")
        {
            WDLoggerUtil.AddLog(" ** Generando CSV: " + filePath);
            //Console.WriteLine(" ** Generando CSV: " + filePath);
            StringBuilder csv = new StringBuilder();
            //get property names from the first object using reflection    
            var propertyInfos = RelevantPropertyInfos<T>(propertyNames);
            IEnumerable<PropertyInfo> props = propertyInfos;// list.First().GetType().GetProperties();

            //header 
            csv.AppendLine(String.Join(strSeparator, props.Select(prop => prop.Name)));
            //rows
            foreach (var entityObject in list)
            {
                csv.AppendLine(String.Join(strSeparator, props.Select(
                    prop => modifiProperty((prop.GetValue(entityObject, null) ?? "").ToString())))
                );
            }
            //put a breakpoint here and check datatable

            System.IO.File.WriteAllText(filePath, csv.ToString(),Encoding.Default);
            return list.Count;
        }

        private static string modifiProperty(string v)
        {
            string replacement = Regex.Replace(v, @"\t|\n|\r", "");
            replacement = Regex.Replace(replacement, @";", ".");
            //replacement = Regex.Replace(replacement, @",", " ");
            return replacement;
        }

        /// <summary>
        /// obtiene la lista de propiedades del objeto que se desean mostrar 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyNames">lista de columnas a mostrar, por defecto se muestran todas</param>
        /// <returns></returns>
        private static List<PropertyInfo> RelevantPropertyInfos<T>(IEnumerable<string> propertyNames= null)
        {
            if (!(object.ReferenceEquals(propertyNames, null))&& propertyNames.Count()>0)
            {
                var propertyInfos = typeof(T).GetProperties().Where(p => propertyNames.Contains(p.Name)).ToDictionary(pi => pi.Name, pi => pi);
                return (from propertyName in propertyNames where propertyInfos.ContainsKey(propertyName) select propertyInfos[propertyName]).ToList();
            }
            else
            {
                var propertyInfos = typeof(T).GetProperties();
                return propertyInfos.ToList();
            }


        }

    }
}
