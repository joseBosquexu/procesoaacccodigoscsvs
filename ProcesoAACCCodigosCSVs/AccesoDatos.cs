﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Net.Mail;
using System.Net;
using System.IO;
using System.Data;
using System.ComponentModel;
using System.Data.Objects.DataClasses;
using System.Reflection;
using System.ServiceModel;
using WDLogger;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace ProcesoAACCCodigosCSVs
{
    public class AccesoDatos
    {
        #region pruebasSinAWS
        /// <summary>
        /// envio del mail sin pasar por el WS.... (Solo para pruebas)....
        /// </summary>
        /// <param name="origen"></param>
        /// <param name="destinatario"></param>
        /// <param name="destinatarioCorreo"></param>
        /// <param name="mensajeHtml"></param>
        /// <param name="ssl"></param>
        /// <param name="mensaje"></param>
        /// <param name="asunto"></param>
        /// <param name="email"></param>
        /// <param name="emaildescripcion"></param>
        /// <param name="idCliente"></param>
        /// <param name="listaAdjuntos"></param>
        /// <param name="listaCopiaCorreo"></param>
        /// <returns></returns>
        public static bool EnviarMail(string origen, string destinatario, string destinatarioCorreo, bool mensajeHtml, bool ssl, string mensaje, string asunto, string email, string emaildescripcion, int? idCliente = null, List<Attachment> listaAdjuntos = null, List<string> listaCopiaCorreo = null)
        {
            bool resposta;

            int port = 587;
            string host = @"email-smtp.eu-west-1.amazonaws.com";
            string password = @"ApUwH0D29ZNXwIACFnOUuZPsGqUEPo/8X2SFDGzerbQ4";
            string username = @"AKIAJG4PDMKGXWEBBG6Q";

            var smtp = new SmtpClient(host, port);
            smtp.Credentials = new NetworkCredential(username, password);

            SmtpClient mail = smtp;
            var msg = new MailMessage();

            msg.From = new MailAddress(email, emaildescripcion);

            msg.To.Add(new MailAddress(destinatarioCorreo, destinatario));
            msg.IsBodyHtml = mensajeHtml;
            msg.Body = mensaje;
            msg.Subject = asunto;

            if (listaAdjuntos != null && listaAdjuntos.Count > 0)
            {
                foreach (var adjunto in listaAdjuntos)
                {
                    msg.Attachments.Add(adjunto);
                }
            }

            if (listaCopiaCorreo != null && listaCopiaCorreo.Count > 0)
            {
                foreach (var correo in listaCopiaCorreo)
                {
                    msg.CC.Add(correo);
                }
            }

            smtp.EnableSsl = ssl;

            try
            {
                mail.Send(msg);
                resposta = true;
            }
            catch (Exception ex)
            {
                resposta = false;
            }
            return resposta;
        }

        /// <summary>
        /// Envia el correo sin utilizar el WS.... (Solo para pruebas)....
        /// Permite Adjuntos
        /// </summary>
        /// <param name="listCSVs"></param>
        /// <param name="listMail"></param>
        public static void EnviarMailCSVs(string[] listCSVs, string[] listMail)
        {
            Console.WriteLine(" ** EnviarMailCSVs AACCCodigosCSVs ** ");
            try
            {

                foreach (string mail in listMail)
                {
                    try
                    {
                        //construimos html
                        StringBuilder htmlEnvio = new StringBuilder();

                        //htmlEnvio.AppendFormat(Recursos.HtmlMail);

                        //Hacemos el envio

                        //EnviarMail("", presentadoraCurso.NOMBRE, presentadoraCurso.EMAIL_1,
                        //    true, true, htmlEnvio.ToString(), "PROMOCION PRESENTADORAS THERMOMIX (R)", "nacho@paraninfo.es", string.Empty);

                        List<Attachment> listaAdjuntos = new List<Attachment>();

                        foreach (string csv in listCSVs)
                        {
                            listaAdjuntos.Add(new Attachment(csv));
                        }
                        EnviarMail("", "jose", mail, true, true, string.Empty, " Pruebas envio pedidos", "suscripciones@thermomixmagazine.com", string.Empty, null, listaAdjuntos);

                    }
                    catch (Exception ex)
                    {
                        //WDLoggerUtil.AddError(" ** ERROR ENVIANDO COMUNICACION: " + ex.Message);
                        Console.WriteLine(" ** ERROR ENVIANDO COMUNICACION: " + ex.Message);

                    }
                }

            }
            catch (Exception ex)
            {
                // WDLoggerUtil.AddError(" ** ERROR ENVIANDO COMUNICACION: " + ex.Message);
                Console.WriteLine(" ** ERROR ENVIANDO COMUNICACION: " + ex.Message);

            }
        }


        #endregion pruebasSinAWS

        #region aTravesDeAWS
        /// <summary>
        /// Envia correo utilizando el WS MailService
        /// No permite adjuntos
        /// </summary>
        /// <param name="asunto"></param>
        /// <param name="contenido"></param>
        /// <param name="correoRemitente"></param>
        /// <param name="listCSVs"></param>
        /// <param name="listMailDestinatarios"></param>
        public static string SendMail(string asunto, string contenido, string correoRemitente, string[] listMailDestinatarios)
        {
            Console.WriteLine(" ** EnviarMailCSVsReference AACCCodigosCSVs ** ");
            try
            {
                MailServiceReference.SendNotificationRequest objSend = new MailServiceReference.SendNotificationRequest();
                objSend.Source = correoRemitente;
                objSend.Body = contenido;
                objSend.Subject = asunto;
                objSend.ToList = listMailDestinatarios;
                objSend.ReplyToList = new string[] { };
                MailServiceReference.MailerServiceClient sendClient = new MailServiceReference.MailerServiceClient();

                string strResult = sendClient.Send(objSend);
                sendClient.Close();
                return strResult;
            }
            catch (FaultException<MailServiceReference.SmtpMailerException> fex)
            {
                //Console.WriteLine(fex);
                return fex.Message;
                //return false;
            }

            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                return ex.Message;
            }
       }

        /// <summary>
        /// Envia correo utilizando el WS MailService
        /// Con archivos adjuntos
        /// </summary>
        /// <param name="asunto"></param>
        /// <param name="contenido"></param>
        /// <param name="correoRemitente"></param>
        /// <param name="listAdjuntos"></param>
        /// <param name="listMailDestinatarios"></param>
        public static string SendMail(string asunto, string texto, bool isHtml, string correoRemitente, string[] listAdjuntos, string[] listMailDestinatarios, string[] listMailCC= null)
        {
            WDLoggerUtil.AddLog(" SendMail:: "+ asunto);
            try
            {
                MailServiceReference.SendNotificationRawRequest objSend = new MailServiceReference.SendNotificationRawRequest();
                objSend.Source = correoRemitente;
                objSend.Body = texto;
                objSend.IsBodyHtml = isHtml;
                objSend.Subject = asunto;
                objSend.ToList = listMailDestinatarios;
                if (listMailCC == null)
                {
                    objSend.CcList = new string[] { };
                }
                else
                {
                    objSend.CcList = listMailCC;
                }
                if (listAdjuntos == null)
                {
                    objSend.Attachments = new string[] { };
                }
                else
                {
                    objSend.Attachments = listAdjuntos;
                }
                objSend.ReplyToList = new string[] { };
                MailServiceReference.MailerServiceClient sendClient = new MailServiceReference.MailerServiceClient();

                string strResult = sendClient.SendRaw(objSend);
                sendClient.Close();
                return strResult;
            }   
            catch (FaultException<MailServiceReference.SmtpMailerException> fex)
            {
                WDLoggerUtil.AddError("FaultException SendMail:: " + fex.Message);
                //Console.WriteLine(fex);
                return fex.Message;
            }

            catch (Exception ex)
            {
                WDLoggerUtil.AddError("Exception SendMail:: " + ex.Message);
                //Console.WriteLine(ex);
                return ex.Message;
            }
        }

        #endregion aTravesDeAWS

        #region dinamicEntity
        public static dynamic getListFromEntity(string tableName)
        {
            try
            {
                using (var MySqlEnt = new mbsuscripcionesEntities())
                {
                    return getListFromEntity(tableName, MySqlEnt);
                }
            }
            catch (Exception)
            {
                try
                {
                    using (var MySqlEnt2 = new nobelsuscripcionesEntities1())
                    {
                        return getListFromEntity(tableName, MySqlEnt2);
                    }
                }
                catch (Exception e)
                {
                    WDLoggerUtil.AddWarning("getListFromEntity unknow type:: " + e.Message);
                    return null;
                }
            }
        }

        private static dynamic getListFromEntity(string tableName, DbContext MySqlEnt)
        {
            ((IObjectContextAdapter)MySqlEnt).ObjectContext.CommandTimeout = 300;
            var type = Assembly.GetExecutingAssembly()
           .GetTypes().FirstOrDefault(t => t.Name == tableName);

            var method = MySqlEnt.GetType().GetMethods()
               .First(x => x.IsGenericMethod && x.Name == "Set");

            MethodInfo generic = method.MakeGenericMethod(type);

            //MethodInfo toListMethod = typeof(Enumerable).GetMethod("ToList");
            //var constructedToList = toListMethod.MakeGenericMethod(type);

            dynamic invokeSet = generic.Invoke(MySqlEnt, null);
            var list = Enumerable.ToList(invokeSet);
            return list;
        }
        #endregion dinamicEntity
    }

}
