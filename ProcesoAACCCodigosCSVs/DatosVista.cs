﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ProcesoAACCCodigosCSVs
{
    /// <summary>
    /// clase encargada de almacenar los datos de las vistas almacenados en el fichero de configuracion.
    /// </summary>
    public class Datos
    {
        /// <summary>
        /// nombre de la vista a tratar
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// asunto del correo a enviar
        /// </summary>
        public string subject { get; set; }

        /// <summary>
        /// cuerpo del correo a enviar
        /// </summary>
        public string body { get; set; }
        /// <summary>
        /// listado de campos pertenecientes a la vista a tratar que se van a mostrar en el csv, por defecto, se muestran todos los campos de la vista
        /// </summary>
        public List<string> Fields { get; set; }

        /// <summary>
        /// lista de direcciones de correo a la que se enviará la vista a tratar
        /// </summary>
        public List<string> Mails { get; set; }

        public Datos()
        {

        }
    }
}
