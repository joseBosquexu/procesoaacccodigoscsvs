//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProcesoAACCCodigosCSVs
{
    using System;
    using System.Collections.Generic;
    
    public partial class viewtemporalpaises
    {
        public short id { get; set; }
        public sbyte id_idioma { get; set; }
        public string nombre { get; set; }
        public float x { get; set; }
        public float y { get; set; }
        public string siglas { get; set; }
        public string id_contiente { get; set; }
        public bool poseeCodigoPostal { get; set; }
    }
}
