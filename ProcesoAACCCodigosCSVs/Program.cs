﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Configuration;
using WDLogger;
using System.Xml.Linq;
using System.Reflection;
using System.Data.Entity;
using System.Runtime.Remoting.Contexts;
using System.Data.Entity.Infrastructure;
using System.Data.Objects.DataClasses;
using System.IO;
using System.Xml;
using System.Collections;

namespace ProcesoAACCCodigosCSVs
{
    class Program
    {

        static void Main(string[] args)
        {

            int idApp = int.Parse(ConfigurationManager.AppSettings["IdApplication"]);
            WDLoggerUtil.StartExecution(idApp);
            WDLoggerUtil.AddLog(" **** COMIENZA LA EJECUCION  AACCCodigosCSVs****");
            if ((args == null)||(args.Length>1))
            {
                WDLoggerUtil.AddLog(" **** ERROR number of arguments AACCCodigosCSVs****");
                WDLoggerUtil.AddLog(" **** How to use: "+ Process.GetCurrentProcess().ProcessName.Split('.')[0]+ " + file_name_list_views" );
            }

            //Console.WriteLine(" ** Main AACCCodigosCSVs ** ");
            try
            {
                List<Datos> datos = workwithXML.readXMl(args[0]);
                foreach (Datos listaVistas in datos)
                {
                    string tableName = listaVistas.Name;
                    string subject = listaVistas.subject;
                    string body = listaVistas.body;
                    List<string> listaCampos = new List<string>();
                    foreach (string field in listaVistas.Fields)
                    {
                        listaCampos.Add(field);
                    }
                    List<string> listaCorreos = new List<string>();
                    foreach (string mail in listaVistas.Mails)
                    {
                        listaCorreos.Add(mail);
                    }
                    WDLoggerUtil.AddLog("Tratando tabla: " + tableName);
                    dynamic list = AccesoDatos.getListFromEntity(tableName);
                    if (!Object.ReferenceEquals(null, list))
                    {
                        int numregs = workWithCsv.WriteToCsv(list, ConfigurationManager.AppSettings["FilePathCSV"] + tableName + ".csv", listaCampos.ToArray());
                        if (numregs > 0)
                        {
                            string result = AccesoDatos.SendMail(subject, body, true, ConfigurationManager.AppSettings["OriginMail"], new string[] { ConfigurationManager.AppSettings["FilePathCSV"] + tableName + ".csv" }, listaCorreos.ToArray());
                            WDLoggerUtil.AddLog("Result send =" + tableName + " " + result);
                        }
                        else
                        {
                            WDLoggerUtil.AddLog("Lista de datos vacia =" + tableName);
                        }
                    }
                    //Console.WriteLine(" Result send = {0}",result);
                }
            }
            catch (Exception ex)
            {
                WDLoggerUtil.AddError("Exception SendMail:: " + ex.Message);
            }
            WDLoggerUtil.AddLog(" ** FIN EJECUCION **");
            WDLoggerUtil.EndExecution();

        }
    }

 
}
