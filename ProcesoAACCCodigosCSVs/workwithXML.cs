﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ProcesoAACCCodigosCSVs
{
    public static class workwithXML
    {
        public static List<Datos> readXMl(string file)
        {
            List<Datos> lista = new List<Datos>();
            //Load xml
            XDocument xdoc = XDocument.Load(file);
            //Run query
            foreach (var vista in xdoc.Descendants("appviews"))
            {
                var lv1s = from lv1 in vista.Elements()
                           select new
                           {
                               Header = lv1.Attribute("id").Value,
                               Subject = lv1.Attribute("subject").Value,
                               Body = lv1.Attribute("body").Value,
                               ChildrenFields = lv1.Descendants("fields"),
                               ChildrenMails = lv1.Descendants("mails")
                           };

                //Loop through results
                foreach (var lv1 in lv1s)
                {
                    Datos dato = new Datos();
                    dato.Name = lv1.Header;
                    dato.body = lv1.Body;
                    dato.subject = lv1.Subject;
                    List<string> listFields = new List<string>();
                    foreach (var lv2 in lv1.ChildrenFields.Descendants("field"))
                        listFields.Add(lv2.Attribute("value").Value);
                    List<string> listMails = new List<string>();
                    foreach (var lv2 in lv1.ChildrenMails.Descendants("mail"))
                        listMails.Add(lv2.Attribute("value").Value);
                    dato.Mails = listMails;
                    dato.Fields = listFields;
                    lista.Add(dato);
                }
            }
            return lista;
        }
    }
}

