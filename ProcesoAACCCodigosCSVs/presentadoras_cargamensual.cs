//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProcesoAACCCodigosCSVs
{
    using System;
    using System.Collections.Generic;
    
    public partial class presentadoras_cargamensual
    {
        public int CODIGO { get; set; }
        public string DNI { get; set; }
        public string NOMBRE { get; set; }
        public string DOMICILIO { get; set; }
        public Nullable<int> POSTAL { get; set; }
        public string POBLACION { get; set; }
        public string PROVINCIA { get; set; }
        public Nullable<int> MOVIL { get; set; }
        public string TFIJO { get; set; }
        public string EMAIL { get; set; }
        public Nullable<System.DateTime> FECHA_ALTA { get; set; }
        public string CODDELE { get; set; }
        public string DESDELE { get; set; }
        public string CODJV { get; set; }
        public string DESJV { get; set; }
    }
}
